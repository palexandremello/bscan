#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
    |||||   ||||||||||||||||| SIMEPAR
    |||||   ||||||||||||||||| Sistema Meteorológico do Paraná
    |||||               |||||
    |||||||||||||||||   ||||| Technology and Environmental Information
    (C) Copyleft Sistema Meteorológico Simepar (SIMEPAR)
        http://www.simepar.br

    Author: Paulo Alexandre da Silva Mello (paulo.mello.simepar@gmail.com)
    =============================
    PLOT_BSCAN_MAP
    =============================

"""
def _check_sweep_in_range(sweep, radar):
    """ Check that a sweep number is in range. """
    if sweep < 0 or sweep >= radar.nsweeps:
        raise IndexError('Sweep out of range: ', sweep)
    return

def get_start(sweep, radar):
    """ Return the starting ray index for a given sweep.  """
    _check_sweep_in_range(sweep, radar)
    return radar.sweep_start_ray_index['data'][sweep]

def get_end(sweep, radar):
    """ Return the ending ray for a given sweep. """
    _check_sweep_in_range(sweep, radar)
    return radar.sweep_end_ray_index['data'][sweep]

def get_start_end(sweep, radar):
    """ Return the starting and ending ray for a given sweep. """
    return get_start(sweep, radar), get_end(sweep, radar)
def get_slice(sweep, radar):
    """ Return a slice for selecting rays for a given sweep. """
    start, end = get_start_end(sweep, radar)
    return slice(start, end+1)

def get_azimuth(sweep, radar,  copy=False):
    """
    Return an array of azimuth angles for a given sweep.
    Parameters
    ----------
    sweep : int
        Sweep number to retrieve data for, 0 based.
    copy : bool, optional
        True to return a copy of the azimuths. False, the default, returns
        a view of the azimuths (when possible), changing this data will
        change the data in the underlying Radar object.
    Returns
    -------
    azimuths : array
        Array containing the azimuth angles for a given sweep.
    """
    s = get_slice(sweep, radar)
    azimuths = radar.azimuth['data'][s]
    if copy:
        return azimuths.copy()
    else:
        return azimuths

def __getData(field,radar,sweep):
    import sys
    try:
        varname = list(radar.fields.keys()).index(field)
        sweep_slice = get_slice(sweep, radar)
        data = radar.fields[field]['data'][sweep_slice]
        longName = radar.fields[field]['long_name']
        dataUnits = radar.fields[field]['units']
        timeUnits = radar.time
        azimuth = get_azimuth(sweep, radar)
        rangeRadar = radar.range['data'] / 1000.
        time=radar.time['units'].split(' ')[2]
        return data, azimuth, rangeRadar
    except ValueError:
        print("{0} doesn't exist on Radar list".format(field))
        sys.exit(1)
def __getUnits(field,radar):
    import sys
    try:
        varname = list(radar.fields.keys()).index(field)
        longName = radar.fields[field]['long_name']
        dataUnits = radar.fields[field]['units']
        timeUnits=radar.time['units'].split(' ')[2]
        azimuthUnits = radar.azimuth['units']
        resolutionData = radar.range['meters_between_gates'] / 1000.
        radarName = radar.metadata['instrument_name']
        
        return longName,dataUnits,timeUnits,azimuthUnits,resolutionData,radarName
    except ValueError:
        print("{0} doesn't exist on Radar list".format(field))
        sys.exit(1)

def __limitsPlot(vmin,vmax):
    return list(range(vmin,vmax+10,5))


def plot_bscan_map(fieldname, radar,sweep,cmap,vmin=None,vmax=None):
    import matplotlib.pyplot as plt
    import pyart
    import matplotlib.colors as colors
    import numpy as np
    field, azimuth, rangeRadar = __getData(fieldname,radar,sweep)
    longName, dataUnits, timeUnits, azimuthUnits, resolutionData, radarName = __getUnits(fieldname, 
    	                                                                                 radar)

    plt.figure(figsize=(6*3.13,4*3.13))
    levs = __limitsPlot(vmin,vmax)
    cp = plt.contourf(rangeRadar,np.sort(azimuth,axis=0),field,levs,cmap=cmap)
    plt.imshow(field, aspect='auto', interpolation='none',
		extent=[0,max(rangeRadar)]+[360,0], origin='lower')
    plt.xlabel('Range (km)')
    plt.ylabel('Azimuth ({0})'.format(azimuthUnits))
    plt.title('{0} {1}'.format(radarName, timeUnits), 
		                   fontsize=12)
    plt.colorbar(cp).set_label('{0} ({1})'.format(longName,
                                                  dataUnits))
    return plt