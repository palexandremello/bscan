#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# bscan/__init__.py
"""
    |||||   ||||||||||||||||| SIMEPAR
    |||||   ||||||||||||||||| Sistema Meteorológico do Paraná
    |||||               |||||
    |||||||||||||||||   ||||| Technology and Environmental Information
    (C) Copyleft Sistema Meteorológico Simepar (SIMEPAR)
        http://www.simepar.br

    Author: Paulo Alexandre da Silva Mello (paulo.mello.simepar@gmail.com)
    =============================
    PLOT_BSCAN_MAP
    =============================

"""
import matplotlib.pyplot as plt
import glob
import sys
import numpy as np
from bscan import MapDisplay
__author__ = "Paulo Alexandre S. Mello"
__email__ = "paulo.mello.simepar@gmail.com"
__date__ = "2018-09-04"
__version__ = '0.0.1'
