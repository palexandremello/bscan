# How to use BSCAN plot

## Using the bscan module:

You can clone the bscan repository from bitbucket:

**$ git clone https://palexandremello@bitbucket.org/palexandremello/bscan.git**

**$ cd bscan**

Now you can import bscan on Python:

**>> import bscan**

Or just run the exampleBSCAN.py:

**$ python exampleBSCAN.py**

## Using the JuPyteR Notebook:

You can use the juypter notebook with the bscan module and see many examples, just clone the bscan repository from bitbucket:

**$ git clone https://palexandremello@bitbucket.org/palexandremello/bscan.git**

**$ cd bscan**

**$ jupyter notebook NotebookBSCAN.ipynb**