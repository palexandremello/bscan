import pyart
import matplotlib.pyplot as plt
import glob
import numpy as np
from bscan import MapDisplay
filenames = glob.glob('TXS180904133107.RAWW7SM')

filenames.sort()
indexVar =  0
indexElev = 0


for filename in filenames:
    radar = pyart.io.sigmet.read_sigmet(filename)
    radarOptsToPlot = {'vars': [key for key in radar.fields.keys()],
                       'elev': radar.sweep_number['data']}

    plt = MapDisplay.plot_bscan_map(radarOptsToPlot['vars'][indexVar], radar,
    	                 sweep=0, cmap='pyart_NWSRef', 
    	                 vmin=10, vmax=60)
    plt.show()
